var express = require('express');
var file = require('fs');
var router = express.Router();
var fs = require('fs')
var path = require('path')

var data = loadDefaultData() || {};

router.get('/episodes', function (req, res) {
  res.json(data)
});

function loadDefaultData () {
  var content = fs.readFileSync(path.resolve(__dirname, 'data.json'), 'utf8');
  return content && JSON.parse(content);
}

module.exports = router;
