import fetch from 'isomorphic-fetch';

export const REQUEST_EPISODES = 'REQUEST_EPISODES';
export const RECEIVE_EPISODES = 'RECEIVE_EPISODES';

function requestEpisodes() {
  return {
    type: REQUEST_EPISODES,
  };
}

function receiveEpisodes(json) {
  return {
    type: RECEIVE_EPISODES,
    episodes: json.results.map((item) => {
      const { id, title } = item;
      return {
        id,
        title,
      };
    }),
  };
}

export function fetchEpisodes() {
  return (dispatch) => {
    dispatch(requestEpisodes());
    return fetch('http://localhost:3000/api/episodes')
      .then(res => res.json())
      .then(json => dispatch(receiveEpisodes(json)));
  };
}
