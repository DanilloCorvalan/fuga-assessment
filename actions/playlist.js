export const PLAYLIST_CREATE = 'PLAYLIST_CREATE';
export const PLAYLIST_REMOVE = 'PLAYLIST_REMOVE';
export const PLAYLIST_ITEM_ADD = 'PLAYLIST_ITEM_ADD';
export const PLAYLIST_ITEM_REMOVE = 'PLAYLIST_ITEM_REMOVE'; // todo

export function createPlaylist(name) {
  return {
    type: PLAYLIST_CREATE,
    name,
  };
}

export function removePlaylist(id) {
  return {
    type: PLAYLIST_REMOVE,
    id,
  };
}

export function addEpisodeToPlaylist(playlistId, episode) {
  return {
    type: PLAYLIST_ITEM_ADD,
    playlistId,
    episode,
  };
}
