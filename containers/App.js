import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { fetchEpisodes } from '../actions/episode';
import { createPlaylist, removePlaylist, addEpisodeToPlaylist } from '../actions/playlist';

import Episodes from '../components/Episodes';
import Playlists from '../components/playlist/';

class App extends Component {
  constructor(props) {
    super(props);
    this.handleNewPlaylist = this.handleNewPlaylist.bind(this);
    this.handlePlaylistRemove = this.handlePlaylistRemove.bind(this);
    this.handleEpisodeAddedToPlaylist = this.handleEpisodeAddedToPlaylist.bind(this);
  }
  componentDidMount() {
    const { dispatch } = this.props;
    dispatch(fetchEpisodes());
  }
  handleNewPlaylist(name) {
    const { dispatch } = this.props;
    dispatch(createPlaylist(name));
  }
  handlePlaylistRemove(id) {
    const { dispatch } = this.props;
    dispatch(removePlaylist(id));
  }
  handleEpisodeAddedToPlaylist(playlistId, episode) {
    console.log('MACUMBA EPISODE ADDED TO PLAYLIST', playlistId, ' ##EP:', episode);
    const { dispatch } = this.props;
    dispatch(addEpisodeToPlaylist(playlistId, episode));
  }
  render() {
    const { episodes, playlists, isFetching } = this.props;
    return (
      <div>
        {isFetching && episodes.length === 0 &&
          <h2>Loading...</h2>
        }
        {!isFetching && episodes.length === 0 &&
          <h2>Empty.</h2>
        }
        {episodes.length > 0 &&
          <div style={{ opacity: isFetching ? 0.5 : 1 }}>
            <Playlists playlists={playlists} onAddPlaylist={this.handleNewPlaylist} onRemovePlaylist={this.handlePlaylistRemove} />
            <Episodes episodes={episodes} playlists={playlists} onAddToPlaylist={this.handleEpisodeAddedToPlaylist} />
          </div>
        }
      </div>
    );
  }
}

App.propTypes = {
  episodes: PropTypes.array.isRequired,
  playlists: PropTypes.array.isRequired,
  isFetching: PropTypes.bool.isRequired,
  dispatch: PropTypes.func.isRequired,
};

function mapStateToProps(state) {
  const { isFetching, episodes } = state.episodes;
  const playlists = state.playlists;
  return {
    isFetching,
    episodes,
    playlists
  };
}

export default connect(mapStateToProps)(App);
