import test from 'tape';
import deepFreeze from 'deep-freeze';

import app from '../reducers/';
import * as actions from '../actions/episode';

test('request episodes', (t) => {
  const stateBefore = {
    episodes: {
      isFetching: false,
    },
  };
  const stateAfter = {
    episodes: {
      isFetching: true,
    },
    playlists: [],
  };

  deepFreeze(stateBefore);
  deepFreeze(stateAfter);

  t.deepEqual(
    app(stateBefore, { type: actions.REQUEST_EPISODES }),
    stateAfter,
  );

  t.end();
});

test('receive episodes', (t) => {
  const episodes = [
    { id: 1, title: 'rahh' },
    { id: 2, title: 'oh yess!' },
  ];
  const stateBefore = {
    episodes: {
      isFetching: true,
      episodes: [],
    },
    playlists: [],
  };
  const stateAfter = {
    episodes: {
      isFetching: false,
      episodes,
    },
    playlists: [],
  };

  deepFreeze(stateBefore);
  deepFreeze(stateAfter);

  t.deepEqual(
    app(stateBefore, { type: actions.RECEIVE_EPISODES, episodes }),
    stateAfter,
  );

  t.end();
});
