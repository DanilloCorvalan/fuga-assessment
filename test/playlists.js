import test from 'tape';
import deepFreeze from 'deep-freeze';

import app from '../reducers/';
import * as actions from '../actions/playlist';

test('create playlist', (t) => {
  const name = 'rah';
  const stateBefore = { playlists: [] };
  const stateAfter = {
    episodes: { episodes: [], isFetching: false },
    playlists: [{ id: 1, items: [], name }],
  };

  deepFreeze(stateBefore);
  deepFreeze(stateAfter);

  t.deepEqual(
    app(stateBefore, actions.createPlaylist(name)),
    stateAfter
  );

  t.end();
});

test('remove playlist', (t) => {
  const name = 'rah';
  const stateBefore = { playlists: [{ id: 1, items: [], name }] };
  const stateAfter = {
    episodes: { episodes: [], isFetching: false },
    playlists: [],
  };

  deepFreeze(stateBefore);
  deepFreeze(stateAfter);

  t.deepEqual(
    app(stateBefore, actions.removePlaylist(1)),
    stateAfter
  );

  t.end();
});

test('add item to playlist', (t) => {
  const name = 'rah';
  const episode = { id: 1, title: 'rahh' };
  const stateBefore = {
    episodes: { episodes: [episode], isFetching: false },
    playlists: [{ id: 1, items: [], name }],
  };
  const stateAfter = {
    episodes: { episodes: [episode], isFetching: false },
    playlists: [{ id: 1, items: [episode], name }],
  };

  deepFreeze(stateBefore);
  deepFreeze(stateAfter);

  t.deepEqual(
    app(stateBefore, actions.addEpisodeToPlaylist(1, episode)),
    stateAfter
  );

  t.end();
});
