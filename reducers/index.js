import { combineReducers } from 'redux';
import episodes from './episode';
import playlists from './playlist';

const rootReducer = combineReducers({
  episodes,
  playlists,
});

export default rootReducer;
