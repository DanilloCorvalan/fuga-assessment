import * as actions from '../actions/episode';

export default function episodes(state = {
  isFetching: false,
  episodes: [],
}, action) {
  switch (action.type) {
    case actions.REQUEST_EPISODES:
      return Object.assign({}, state, {
        isFetching: true,
      });
    case actions.RECEIVE_EPISODES:
      return Object.assign({}, state, {
        isFetching: false,
        episodes: action.episodes,
      });
    default:
      return state;
  }
}
