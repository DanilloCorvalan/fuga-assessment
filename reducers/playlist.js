import * as actions from '../actions/playlist';
let _id = 1;

const playlist = (state, action) => {
  switch (action.type) {
    case actions.PLAYLIST_CREATE:
      return {
        name: action.name,
        id: _id++,
        items: [],
      };
    case actions.PLAYLIST_ITEM_ADD: {
      if (state.id.toString() !== action.playlistId.toString()) {
        return state;
      }
      return Object.assign({}, state, {
        items: [...state.items, action.episode],
      });
    }
    default:
      return state;
  }
};

export default function playlists(state = [], action) {
  switch (action.type) {
    case actions.PLAYLIST_CREATE:
      return [
        ...state,
        playlist(undefined, action),
      ];
    case actions.PLAYLIST_REMOVE: {
      const index = state.findIndex(p => p.id === action.id);
      if (index === -1) {
        return state;
      }
      return [
        ...state.slice(0, index),
        ...state.slice(index + 1),
      ];
    }
    case actions.PLAYLIST_ITEM_ADD:
      return state.map(p =>
        playlist(p, action)
      );
    default:
      return state;
  }
}
