# React redux example

## Start
  - run `npm install`
  - run `npm start`
  - open browser on `http://localhost:3000`


## Tests
  - run `npm test`

## Todo:
  - async tests using `redux-mock-store` and `nock`
  - use `normalizr` to avoid storing the whole objects in state (e.g.: playlist episodes)
  - separate better **presentational** and **container** components
  - Layout is pretty ugly :D
