import React, { Component, PropTypes } from 'react';
import PlaylistsSelect from '../components/playlist/select';

export default class Episodes extends Component {
  render() {
    const { episodes, playlists, onAddToPlaylist } = this.props;
    return (
      <div>
        <h1>Episodes!</h1>
        <ul className="episodes">
          {episodes.map((episode, i) =>
            <li key={i}>
              <h1>{episode.title}</h1>
              <PlaylistsSelect playlists={playlists} onAddToPlaylist={onAddToPlaylist} episode={episode} />
            </li>
          )}
        </ul>
      </div>
    );
  }
}

Episodes.propTypes = {
  episodes: PropTypes.array.isRequired,
  playlists: PropTypes.array.isRequired,
  onAddToPlaylist: PropTypes.func.isRequired,
};
