import React, { Component, PropTypes } from 'react';
import AddPlaylist from './add.js';
import List from './list.js';

export default class Playlists extends Component {
  render() {
    const { playlists, onAddPlaylist, onRemovePlaylist } = this.props;
    return (
      <div>
        <h1>Playlists!</h1>
        <AddPlaylist onAddPlaylist={onAddPlaylist} />
        <List playlists={playlists} onRemovePlaylist={onRemovePlaylist} />
      </div>
    );
  }
}

Playlists.propTypes = {
  playlists: PropTypes.array.isRequired,
  onAddPlaylist: PropTypes.func.isRequired,
  onRemovePlaylist: PropTypes.func.isRequired,
};
