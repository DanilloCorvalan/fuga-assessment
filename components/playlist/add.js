import React, { Component, PropTypes } from 'react';

export default class AddPlaylist extends Component {
  render() {
    let input;
    const { onAddPlaylist } = this.props;
    return (
      <div className="addPlaylist">
        <form onSubmit={e => {
          e.preventDefault();
          if (!input.value.trim()) {
            return;
          }
          onAddPlaylist(input.value);
          input.value = '';
        }}>
          <input ref={node => {
            input = node;
          }} />
          <button type="submit">
            Add Playlist
          </button>
        </form>
      </div>
    );
  }
}


AddPlaylist.propTypes = {
  onAddPlaylist: PropTypes.func.isRequired,
};
