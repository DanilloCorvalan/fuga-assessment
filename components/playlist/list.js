import React, { Component, PropTypes } from 'react'

export default class PlaylistList extends Component {
  render() {
    const { playlists, onRemovePlaylist } = this.props;
    return (
      <ul className="playlists">
        {playlists.map((playlist, i) =>
          <li key={'playlist-' + i}>
            <h1>
              {playlist.name}
              <button onClick={onRemovePlaylist.bind(null, playlist.id)}>Remove</button>
            </h1>
            <ul className="items">
              {playlist.items.map((item, j) =>
                <li key={'item-' + j}>{item.title}</li>
              )}
            </ul>
          </li>
        )}
      </ul>
    );
  }
}


PlaylistList.propTypes = {
  playlists: PropTypes.array.isRequired,
  onRemovePlaylist: PropTypes.func.isRequired,
};
