import React, { Component, PropTypes } from 'react';


function getPlaylistsWhitoutEpisode(playlists, episode) {
  return playlists.filter((playlist) =>
    playlist.items.findIndex((item) => episode.id.toString() === item.id.toString()) === -1
  );
}

export default class PlaylistsSelect extends Component {
  render() {
    const { playlists, episode, onAddToPlaylist } = this.props;
    const visiblePlaylists = getPlaylistsWhitoutEpisode(playlists, episode);

    return (
      <div>
      <select onChange={e => onAddToPlaylist(e.target.value, episode)}>
      <option value="">Add to Playlist:</option>
      {visiblePlaylists.map((playlist) =>
        <option key={playlist.id} value={playlist.id}>{playlist.name}</option>
      )}
      </select>
      </div>
    );
  }
}

PlaylistsSelect.propTypes = {
  playlists: PropTypes.array.isRequired,
  onAddToPlaylist: PropTypes.func.isRequired,
  episode: PropTypes.shape({
    id: PropTypes.number,
    title: PropTypes.string,
  }),
};
